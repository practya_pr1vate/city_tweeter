<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('map');
});


Route::get('/g', function () {
    return view('g_test');
});
//Auth::routes();
//
Route::post('/search_city', 'MapController@search_city');
//
//Auth::routes();
//
//Route::get('/home', 'HomeController@index')->name('home');
