<?php
/**
 * @package    API Library
 * @author     Yudhistira Eka Putra
 * @version    1.0
 */

namespace App;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use Log;

class Api
{

    public function __construct()
    {
        $this->google_client = new Client(['base_uri' => env('GOOGLE_URI')]);
        $this->google_api_key = env('GOOGLE_API_KEY');
    }
    public function search_city_name($search)
    {
        $response = $this->google_client->request('GET', 'maps/api/place/autocomplete/json?input=' . $search . '&types=(regions)&key=' . $this->google_api_key, ['verify' => false]);
        $data_response = json_decode($response->getBody());

        return $data_response;
    }

    public function search_coordinator($place_id)
    {
        $location = $this->google_client->request('GET', 'maps/api/place/details/json?placeid=' . $place_id . '&key=' . $this->google_api_key, ['verify' => false]);
        $get_localtion = json_decode($location->getBody());
        return $get_localtion;
    }

}
