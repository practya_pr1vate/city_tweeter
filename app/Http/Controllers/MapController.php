<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use GuzzleHttp\Client;
use Log;

use Illuminate\Support\Carbon;
use Session;
use App\Storages\Twitter;
use App\Storages\Place;
use App\Api;

class MapController extends Controller
{

    public function search_city(Request $request)
    {

        $input = $request->get('city_name');
        $api = new Api();
        $place = $api->search_city_name($input);

        if ($place->status == 'OK') {
            $session = array();
            if (Session::get('city_name') != "") {
                $session = Session::get('city_name');
            }
            $place_id = $place->predictions[0]->place_id;

            $data = $this->select_city($place, $session);

            return $data;


        } else {
            return response()->json(['success' => false]);

        }

    }

    public function select_city($place, $session)
    {
        $place_id = $place->predictions[0]->place_id;
        $find_place = Place::find_place($place_id);

        if (empty($find_place) == false) {

            $current = Carbon::now();
            $interval = $current->diffInMinutes($find_place->search_time);

            if ($interval > 60) {
                //Call Twitter API and update search_time
                $users = Twitter::get_tweet_user($place_id);
                array_push($session, $find_place->name);
                Session::put('city_name', $session);

            } else {
                $users = Twitter::get_tweet_user($place_id);
                array_push($session, $find_place->name);
                Session::put('city_name', $session);

            }
            return response()->json(['success' => true,
                'place_id' => $place_id,
                'city' => $find_place->name,
                'lat' => $find_place->lat,
                'lng' => $find_place->lng,
                'users' => $users,
                'session' => $session
            ]);
        } else {
            $api = new Api();
            $get_localtion = $api->search_coordinator($place_id);
            $place_data = array(
                'place_id' => $place_id,
                'name' => $place->predictions[0]->structured_formatting->main_text,
                'lat' => $get_localtion->result->geometry->location->lat,
                'lng' => $get_localtion->result->geometry->location->lng
            );
            Place::insert_place($place_data);

            //Call Twitter API and Insert users
            array_push($session, $place->predictions[0]->structured_formatting->main_text);
            Session::put('city_name', $session);
            return response()->json(['success' => true,
                'place_id' => $place_id,
                'city' => $place->predictions[0]->structured_formatting->main_text,
                'lat' => $get_localtion->result->geometry->location->lat,
                'lng' => $get_localtion->result->geometry->location->lng,
                'session' => $session,
//                'users' => $users
            ]);


        }

    }


}
