<?php
/**
 * @package    API
 * @author     Yudhistira Eka Putra
 */

namespace App\Storages;

use DateTime;
use Illuminate\Support\Facades\DB;
use Log;

class Twitter {

    static function get_tweet_user($place_id)
    {        $users = DB::table('tw_user')
            ->where('place_id', '=', $place_id)
            ->get();
        return $users;
    }

}
