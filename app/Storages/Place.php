<?php
/**
 * @package    API
 * @author     Yudhistira Eka Putra
 */

namespace App\Storages;

use DateTime;
use Illuminate\Support\Facades\DB;
use Log;

class Place {

    static function find_place($place_id)
    {
        $find_place = DB::table('place')
            ->where('place_id', '=', $place_id)
            ->first();
        return $find_place;
    }
    static function insert_place($place_data)
    {
        DB::table('place')->insert($place_data);
    }
}
