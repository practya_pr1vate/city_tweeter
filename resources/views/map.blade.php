<!DOCTYPE html>
<html>
<head>
    <title>City Tweeter</title>
    <meta name="viewport" content="initial-scale=1.0, user-scalable=no">
    <meta charset="utf-8">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css?v=1" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
    <style>
        /* Always set the map height explicitly to define the size of the div
         * element that contains the map. */
        #map {
            height: 80%;
            width: 100%;
            margin: auto;
        }

        /* Optional: Makes the sample page fill the window. */

    </style>
    <style>
        html, body {
            background-color: #fff;
            color: #636b6f;
            font-family: 'Nunito', sans-serif;
            font-weight: 200;
            height: 100vh;
            margin: 0;
        }

        .links > a {
            color: #636b6f;
            padding: 0 25px;
            font-size: 13px;
            font-weight: 600;
            letter-spacing: .1rem;
            text-decoration: none;
            text-transform: uppercase;
        }
        .dropdown {
            position: inherit;
            display: inline-block;
        }

        .dropdown-content {
            display: none;
            position: absolute;
            background-color: #f1f1f1;
            min-width: 160px;
            overflow: auto;
            box-shadow: 0px 8px 16px 0px rgba(0, 0, 0, 0.2);
            z-index: 1;
        }

        .dropdown-content div {
            color: black;
            padding: 12px 16px;
            text-decoration: none;
            display: block;
        }

        .dropdown div:hover {
            background-color: #ddd;
            cursor: pointer;
        }

        form {
            display: contents;
        }


    </style>

</head>
<body>


<div id="map"></div>

<form method="POST" action="{{ URL::to('search_city')}}" id="form-search">
    {{ csrf_field() }}
    <input type="text" id="city_name" name="city_name" placeholder="City Name">
    <button type="button" id='search' name='search' class="btn btn-primary">
        Search
    </button>


</form>

<div class="dropdown">
    <button onclick="myFunction()" class="dropbtn btn btn-primary">History</button>
    <div id="his_drop" class="dropdown-content">
        @if(!empty(Session::get('city_name')))
            @for ($i = count(Session::get('city_name'))-1; $i >= 0; $i--)
                <div onclick="select_drop('{{ Session::get('city_name')[$i] }}')">{{ Session::get('city_name')[$i] }}</div>
            @endfor
        @endif
    </div>
</div>
<script type="text/javascript" src="http://code.jquery.com/jquery-1.7.1.min.js"></script>
<script type="text/javascript" src="http://malsup.github.com/jquery.form.js"></script>
{{--<meta name="viewport" content="width=device-width, initial-scale=1">--}}
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">


<script>
    // This example displays a map with the language and region set
    // to Japan. These settings are specified in the HTML script element
    // when loading the Google Maps JavaScript API.
    // Setting the language shows the map in the language of your choice.
    // Setting the region biases the geocoding results to that region.
    function initMap() {
        var map = new google.maps.Map(document.getElementById('map'), {
            zoom: 10,
            center: {lat: 13.7563, lng: 100.5018},
            streetViewControl: false
        });
    }
</script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyARVn4OHQe_jbyGSRiKXyViIUgWaKo1zew&callback=initMap&language=en"
        async defer>
</script>
<script type="text/javascript">

    var markers = [];


    $('#history').click(function () {

    });
    $('#search').click(function () {
        $('#form-search').submit();

    })
    $('#form-search').submit(function (e) {

        $(this).ajaxSubmit({
            error: function (response) {
                var errors = $.parseJSON(response.responseText);
            },
            success: function (response) {
                if (response.success == true) {
                    // getLatLngFromAddress(response.description);
                    var map = new google.maps.Map(document.getElementById('map'), {
                        zoom: 10,
                        center: {lat: parseFloat(response.lat), lng: parseFloat(response.lng)},
                        streetViewControl: false

                    });
                    var features = []

                    var amount = 0
                    if (response.users != null) {
                        amount =  response.users.length
                    }

                    for (var i = 0; i < amount; i++) {
                        var fe = {
                            position: new google.maps.LatLng(response.users[i].lat, response.users[i].lng),
                        }
                        features.push(fe);
                    }


                    for (var i = 0; i < features.length; i++) {

                        var content = '<b>' + response.users[i].name + '</b><br>' +
                            response.users[i].tweet + '<br>' +
                            response.users[i].hashtag + '<br>' +
                            response.users[i].date_time


                        var infowindow = new google.maps.InfoWindow({
                            content: content
                        });

                        var marker = new google.maps.Marker({
                            position: features[i].position,
                            icon: response.users[i].img,
                            map: map

                        });

                        google.maps.event.addListener(marker, 'click', (function (marker, content, infowindow) {
                            return function () {
                                infowindow.setContent(content);
                                infowindow.open(map, marker);
                            };
                        })(marker, content, infowindow));

                    }
                    ;

                    var his_div = '';
                    for (var i = response.session.length - 1; i >= 0; i--) {
                        his_div = his_div + '<div onclick=\"select_drop(\'' + response.session[i] + '\')\">' + response.session[i] + '</div>';
                    }
                    document.getElementById('his_drop').innerHTML = his_div;


                }
                else {


                }
            }
        });

        return false;
    });

    function select_drop(city_select) {
        document.getElementById("city_name").value = city_select;
        $('#form-search').submit();
    }

    function myFunction() {
        document.getElementById("his_drop").classList.toggle("show");
    }


    // Close the dropdown if the user clicks outside of it
    window.onclick = function (event) {
        if (!event.target.matches('.dropbtn')) {
            var dropdowns = document.getElementsByClassName("dropdown-content");
            var i;
            for (i = 0; i < dropdowns.length; i++) {
                var openDropdown = dropdowns[i];
                if (openDropdown.classList.contains('show')) {
                    openDropdown.classList.remove('show');
                }
            }
        }
    }
</script>

</body>
</html>
