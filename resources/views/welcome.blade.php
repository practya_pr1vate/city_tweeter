
<style>
    div.content div:nth-child(1){
        color: red;
    }
    div.content div:nth-child(2),div:nth-child(3){
        background-color: blue;
        font-weight: bold;
        border: 1px solid green;
    }
    div.content div:nth-child(3){
        text-decoration: underline;
    }
</style>


<script src="jquery/2.1.1/jquery.min.js"></script>
<script type="text/javascript">


    setTimeout(function(){
        var content = document.getElementsByClassName('content')[0];
        var height = content.clientHeight;
        for (var i = 0; i < content.children.length; i++) {
            content.children[i].setAttribute("style","height:"+ height+"px");
        }
    }, 1);

</script>
<div class="content">
    <div>Content 1</div>
    <div>Content 2</div>
    <div>Content 3</div>
    <div>Content 4</div>
</div>
